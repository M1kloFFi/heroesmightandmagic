import Interface.heroSkils;
import race.saveLog;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static race.elf.ElfSquad.createElfSquad;
import static race.human.humanSquad.createHumanSquad;
import static race.orc.orcSquad.createOrcSquad;
import static race.saveLog.saveLogStep;

import static race.undead.undeadSquad.createUndeadSquad;


public class Main {
    static boolean win;

    public static void main(String[] args) throws FileNotFoundException {
        saveLog testq = new saveLog();
        List<heroSkils> alianceSquad = null;
        List<heroSkils> hordeSquad = null;

        int alianceChoiseSquad = (0 + (int) (Math.random() * 2));
        switch (alianceChoiseSquad) {
            case 0:
                alianceSquad = createElfSquad();
                break;
            case 1:
                alianceSquad = createHumanSquad();
                break;
        }

        int hordeChoiseSquad = (0 + (int) (Math.random() * 2));
        switch (hordeChoiseSquad) {
            case 0:
                hordeSquad = createOrcSquad();
                break;
            case 1:
                hordeSquad = createUndeadSquad();
                break;
        }
        fighrSquad(alianceSquad, hordeSquad);
    }

    public static void fighrSquad(List<heroSkils> alianceSquad, List<heroSkils> hordeSquad) {
        int count = 1;

        while (!win) {

            if (alianceSquad.size() != 0 && hordeSquad.size() != 0) {
                saveLogStep("Фаза боя №" + count + " " + alianceSquad.get(0).race() + "\r\n");
                count++;
                stepFight(alianceSquad, hordeSquad);
            }
            if (hordeSquad.size() != 0 && alianceSquad.size() != 0) {
                saveLogStep("Фаза боя №" + count + " " + hordeSquad.get(0).race() + "\r\n");
                count++;
                stepFight(hordeSquad, alianceSquad);
            }
        }
    }

    public static void stepFight(List<heroSkils> team1, List<heroSkils> team2) {
        double damage;
        int whoAtacked;
        List<Integer> move, move2, elitGrout = new ArrayList<>();

        move = move(team1.size());

        for (int i = 0; i < team1.size(); i++) {
            if (team1.get(i).buffCheck() == true) {
                elitGrout.add(i);
            }
        }

        for (int i = 0; i < team1.size(); i++) {
            if (elitGrout.isEmpty() != true) {
                damage = team1.get(elitGrout.get(0)).attack();
                for (int j = 0; j < move.size() ; j++) {
                    if(move.get(j) == elitGrout.get(0)){
                        move.remove(j);
                        elitGrout.remove(0);
                        move = move(team1.size());
                        break;
                    }
                }
            } else damage = team1.get(move.get(i)).attack();
            if (damage == 0) {
                whoAtacked = rand(team1);
                team1.get(move.get(whoAtacked)).buffToEliteHero();
                team1.get(move.get(whoAtacked)).classHero();
                saveLogStep("\r\n");
                continue;
            }
            if (damage == -1 || damage == -2) {
                move2 = move(team2.size());
                whoAtacked = rand(team2);
                if (damage == -1)
                    team2.get(move2.get(whoAtacked)).deBuffToEliteHero();
                else
                    team2.get(move2.get(whoAtacked)).deBuffHero();
                team2.get(move2.get(whoAtacked)).classHero();
                saveLogStep("\r\n");
                continue;
            }

            whoAtacked = 0 + (int) (Math.random() * team2.size());
            team2.get(whoAtacked).classHero();
            saveLogStep(" на " + damage + " урона" + "\r\n");
            team2.get(whoAtacked).damage(damage);
            if (team2.get(whoAtacked).health() <= 0) {
                team2.get(whoAtacked).deathHero();
                team2.remove(whoAtacked);
            }
            if (team2.size() == 0) {
                switch (team1.get(0).race()){
                    case "Орда":
                        saveLogStep("\r\nПобедила " + team1.get(0).race());
                        break;
                    case "Альянс":
                        saveLogStep("\r\nПобедил " + team1.get(0).race());
                        break;
                }
                win = true;
                return;
            }
        }
    }

    public static int rand(List<heroSkils> list) {
        return (0 + (int) (Math.random() * list.size()));
    }

    public static List move(int num) {
        List<Integer> move = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            move.add(i);
        }
        Collections.shuffle(move);
        return move;
    }
}








