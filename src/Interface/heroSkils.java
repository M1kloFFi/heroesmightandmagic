package Interface;

public interface heroSkils {
    void classHero();
    void deathHero();
    void buffToEliteHero();
    void deBuffToEliteHero();
    void deBuffHero();
    void damage(double damage);
    boolean buffCheck();
    int health();
    double attack();
    String race();
    String[] bodyParts  = {
            " в голову ",
            " в туловище ",
            " в левую руку ",
            " в правую руку ",
            " в левую ногу ",
            " в правую ногу ",
            " в шею ",
    };
}
