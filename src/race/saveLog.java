package race;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class saveLog {
    static FileOutputStream fos;
    static PrintStream ps;

    public saveLog() {
        try {
            fos = new FileOutputStream("GameStat.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ps = new PrintStream(fos, false);
    }

    public static void saveLogStep(String text) {
        System.out.print(text);
        ps.print(text);
    }
}
