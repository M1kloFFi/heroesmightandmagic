package race.human;


import Interface.heroSkils;
import race.allianceHero;

import static race.saveLog.saveLogStep;

public class humanArcher extends allianceHero implements heroSkils {
    //стрелять из арбалета (нанесение урона 5 HP)
    //атаковать (нанесение урона 3 HP)
    public double attack() {
        int act = (0 + (int) (Math.random() * 2));
        int randBody = (0 + (int) (Math.random() * 7));
        switch (act) {
            case 0:
                if (deBuff == true)
                    deBuffHero = 2;
                saveLogStep("Человек Арбалетчик делает выстрел из лука " + bodyParts[randBody]);
                if (ellitGroup == true) {
                    ellitGroup = false;
                    deBuff = false;
                    return 5.0 * 1.5 / deBuffHero;
                } else {
                    deBuff = false;
                    return 5 / deBuffHero;
                }
            default:
                if (deBuff == true)
                    deBuffHero = 2;
                saveLogStep("Человек Арбалетчик наносит удар клинком " + bodyParts[randBody]);
                if (ellitGroup == true) {
                    ellitGroup = false;
                    deBuff = false;
                    return 3.0 * 1.5 / deBuffHero;
                } else{ deBuff = false; return 3 / deBuffHero;}
        }
    }

    public void damage(double damage) {
        this.heals -= damage;
    }

    public int health() {
        //System.out.println("Остаток здоровья Человек Арбалетчик: : " + this.heals + "HP");
        return this.heals;
    }

    public String race() {
        return "Альянс";
    }

    public void classHero() {
        saveLogStep("Человек арбалетчик");
    }

    public void deathHero() {
        saveLogStep("Человек арбалетчик погиб в бою\n");
    }

    public void buffToEliteHero() {
        ellitGroup = true;
    }

    public void deBuffToEliteHero() {
        ellitGroup = false;
    }

    public boolean buffCheck() {
        return ellitGroup;
    }

    public void deBuffHero() {
        deBuff = true;
    }
}
