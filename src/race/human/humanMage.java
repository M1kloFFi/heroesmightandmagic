package race.human;

import Interface.heroSkils;
import race.allianceHero;

import static race.saveLog.saveLogStep;

public class humanMage extends allianceHero implements heroSkils {
    //наложение улучшения на персонажа своего отряда.
//    атаковать магией (нанесение урона 4 HP)
    public double attack() {
        int act = (0 + (int) (Math.random() * 2));
        int randBody = (0 + (int) (Math.random() * 7));
        switch (act) {
            case 0:
                if (deBuff == true)
                    deBuffHero = 2;
                saveLogStep("Человек Маг атакует магией ");
                if (ellitGroup == true) {
                    ellitGroup = false;
                    return 4 * 1.5 / deBuffHero;
                } else {
                    deBuff = false;
                    return 4 / deBuffHero;
                }
            default:
                saveLogStep("Человек Маг накладывает улучшение на ");
                if (ellitGroup == true) {
                    ellitGroup = false;
                    deBuff = false;
                    return 0;
                } else {
                    deBuff = false;
                    return 0;
                }
        }
    }

    public void damage(double damage) {
        this.heals -= damage;
    }

    public int health() {
        return this.heals;
    }

    public String race() {
        return "Альянс";
    }

    public void classHero() {
        saveLogStep("Человек маг");
    }

    public void deathHero() {
        saveLogStep("Человек маг погиб в бою\n");
    }

    public void buffToEliteHero() {
        ellitGroup = true;
    }

    public void deBuffToEliteHero() {
        ellitGroup = false;
    }

    public boolean buffCheck() {
        return ellitGroup;
    }

    public void deBuffHero() {
        deBuff = true;
    }

}
