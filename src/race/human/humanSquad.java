package race.human;

import Interface.heroSkils;
import java.util.ArrayList;
import java.util.List;


public class humanSquad {
    public static List createHumanSquad() {
        List<heroSkils> list = new ArrayList<heroSkils>();
        list.add(new humanMage());
        list.add(new humanArcher());
        list.add(new humanArcher());
        list.add(new humanArcher());
        list.add(new humanWarrior());
        list.add(new humanWarrior());
        list.add(new humanWarrior());
        list.add(new humanWarrior());
        return list;
    }
}
