package race.elf;
import Interface.*;
import java.util.ArrayList;
import java.util.List;


public class ElfSquad {
    public static List createElfSquad() {
        List<heroSkils> list = new ArrayList<heroSkils>();
        list.add(new ElfMage());
        list.add(new ElfArcher());
        list.add(new ElfArcher());
        list.add(new ElfArcher());
        list.add(new ElfWarrior());
        list.add(new ElfWarrior());
        list.add(new ElfWarrior());
        list.add(new ElfWarrior());
        return list;
    }
}
