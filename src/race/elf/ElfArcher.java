package race.elf;

import Interface.heroSkils;
import race.allianceHero;

import static race.saveLog.saveLogStep;

public class ElfArcher extends allianceHero implements heroSkils {
    //стрелять из лука (нанесение урона 7 HP)
    //атаковать противника (нанесение урона 3 HP)
    public double attack() {
        int act = (0 + (int) (Math.random() * 2));
        int randBody = (0 + (int) (Math.random() * 7));
        if (deBuff == true)
            deBuffHero = 2;
        switch (act) {
            case 0:
                saveLogStep("Эльф Лучник делает точный выстрел из лука " + bodyParts[randBody]);
                if (ellitGroup == true) {
                    ellitGroup = false;
                    deBuff = false;
                    return 7.0 * 1.5 / deBuffHero;
                } else {
                    deBuff = false;
                    return 7 / deBuffHero;
                }
            default:
                saveLogStep("Эльф Лучник наносит удар кинжалом " + bodyParts[randBody]);
                if (ellitGroup == true) {
                    ellitGroup = false;
                    deBuff = false;
                    return 3.0 * 1.5 / deBuffHero;
                } else {
                    deBuff = false;
                    return 3 / deBuffHero;
                }
        }
    }

    public void damage(double damage) {
        this.heals -= damage;
    }

    public int health() {
        return this.heals;
    }

    public String race() {
        return "Альянс";
    }

    public void classHero() {
        saveLogStep("Эльф лучник");
    }

    public void deathHero() {
        saveLogStep("Эльф лучник погиб в бою\n");
    }

    public void buffToEliteHero() {
        ellitGroup = true;
    }

    public void deBuffToEliteHero() {
        ellitGroup = false;
    }

    public boolean buffCheck() {
        return ellitGroup;
    }

    public void deBuffHero() {
        deBuff = true;
    }
}
