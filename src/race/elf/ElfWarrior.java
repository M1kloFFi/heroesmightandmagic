package race.elf;

import Interface.heroSkils;
import race.allianceHero;

import static race.saveLog.saveLogStep;

public class ElfWarrior extends allianceHero implements heroSkils {
    //атаковать мечом (нанесение урона 15 HP)
    public double attack() {
        int randBody = (0 + (int) (Math.random() * 7));
        if (deBuff == true)
            deBuffHero = 2;
        saveLogStep("Эльф Воин наносит удар мечом" + bodyParts[randBody]);
        if (ellitGroup == true){
            ellitGroup = false;
            deBuff = false;
            return 15 * 1.5 / deBuffHero;}
        else {
            deBuff = false;
            return 15 / deBuffHero;
        }

    }

    public void damage(double damage) {
        this.heals -= damage;
    }

    public int health() {
        return this.heals;
    }

    public String race() {
        return "Альянс";
    }

    public void classHero() {
        saveLogStep("Эльф воин");
    }

    public void deathHero() {
        saveLogStep("Эльф воин погиб в бою\n");
    }

    public void buffToEliteHero() {
        ellitGroup = true;
    }

    public void deBuffToEliteHero() {
        ellitGroup = false;
    }

    public boolean buffCheck() {
        return ellitGroup;
    }

    public void deBuffHero() {
        deBuff = true;
    }
}
