package race.elf;

import Interface.heroSkils;
import race.allianceHero;

import static race.saveLog.saveLogStep;

public class ElfMage extends allianceHero implements heroSkils {
    //наложение улучшения на персонажа своего отряда
    //нанесение урона персонажу противника магией на 10 HP
    public double attack() {
        int act = (0 + (int) (Math.random() * 2));
        if (deBuff == true)
            deBuffHero = 2;
        switch (act) {
            case 0:
                saveLogStep("Эльф Маг атакует магией природы ");
                if (ellitGroup == true) {
                    ellitGroup = false;
                    deBuff = false;
                    return 10 * 1.5 / deBuffHero;
                } else return 10 / deBuffHero;
            default:
                saveLogStep("Эльф Маг накладывает улучшение на ");
                deBuff = false;
                return 0;
        }
    }

    public void damage(double damage) {
        this.heals -= damage;
    }

    public int health() {
        return heals;
    }

    public String race() {
        return "Альянс";
    }

    public void classHero() {
        saveLogStep("Эльф маг");
    }

    public void deathHero() {
        saveLogStep("Эльф маг погиб в бою\n");
    }

    public void buffToEliteHero() {
        ellitGroup = true;
    }

    public boolean buffCheck() {
        return ellitGroup;
    }

    public void deBuffHero() {
        deBuff = true;
    }

    public void deBuffToEliteHero() {
        ellitGroup = false;
    }
}
