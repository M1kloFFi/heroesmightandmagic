package race.undead;

import Interface.heroSkils;
import race.hordeHero;

import static race.saveLog.saveLogStep;

public class undeadMage extends hordeHero implements heroSkils {
    //наслать недуг (уменьшение силы урона персонажа противника на 50% на один ход)
    //атака (нанесение урона 5 HP)
    public double attack() {
        int act = (0 + (int) (Math.random() * 2));
        int randBody = (0 + (int) (Math.random() * 7));
        switch (act) {
            case 0:
                saveLogStep("Нежить Шаман накладывает недуг на ");
                if (ellitGroup == true) {
                    ellitGroup = false;
                    return -2;
                } else return -2;
            default:
                saveLogStep("Нежить Шаман атакует магией ");
                if (ellitGroup == true) {
                    ellitGroup = false;
                    return 5 * 1.5;
                } else return 5;
        }
    }

    public void damage(double damage) {
        this.heals -= damage;
    }

    public int health() {
        return this.heals;
    }

    public String race() {
        return "Орда";
    }

    public void classHero() {
        saveLogStep("Нежить шаман");
    }

    public void deathHero() {
        saveLogStep("Нежить Шаман погиб в бою\n");
    }

    public void buffToEliteHero() {
        ellitGroup = true;
    }

    public void deBuffToEliteHero() {
        ellitGroup = false;
    }

    public boolean buffCheck() {
        return ellitGroup;
    }

    public void deBuffHero() {
    }
}
