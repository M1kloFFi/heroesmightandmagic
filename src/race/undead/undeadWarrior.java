package race.undead;

import Interface.heroSkils;
import race.hordeHero;

import static race.saveLog.saveLogStep;

public class undeadWarrior extends hordeHero implements heroSkils {
    //удар копьем (нанесение урона 18 HP)
    public double attack() {
        int randBody = (0 + (int) (Math.random() * 7));
        saveLogStep("Нежить Зомби наносит удар копьем" + bodyParts[randBody]);
        if (ellitGroup == true) {
            ellitGroup = false;
            return 18 * 1.5;
        } else return 18;
    }

    public void damage(double damage) {
        this.heals -= damage;
    }

    public int health() {
        return this.heals;
    }

    public String race() {
        return "Орда";
    }

    public void classHero() {
        saveLogStep("Нежить зомби");
    }

    public void deathHero() {
        saveLogStep("Нежить зомби погиб в бою\n");
    }

    public void buffToEliteHero() {
        ellitGroup = true;
    }

    public void deBuffToEliteHero() {
        ellitGroup = false;
    }

    public boolean buffCheck() {
        return ellitGroup;
    }

    public void deBuffHero() {
    }

}
