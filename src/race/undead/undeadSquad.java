package race.undead;

import Interface.heroSkils;
import java.util.ArrayList;
import java.util.List;


public class undeadSquad {
    public static List createUndeadSquad() {
        List<heroSkils> list = new ArrayList<heroSkils>();
        list.add(new undeadMage());
        list.add(new undeadArcher());
        list.add(new undeadArcher());
        list.add(new undeadArcher());
        list.add(new undeadWarrior());
        list.add(new undeadWarrior());
        list.add(new undeadWarrior());
        list.add(new undeadWarrior());
        return list;
    }
}
