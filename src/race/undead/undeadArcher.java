package race.undead;

import Interface.heroSkils;
import race.hordeHero;

import static race.saveLog.saveLogStep;

public class undeadArcher extends hordeHero implements heroSkils {
    //стрелять из лука (нанесение урона 4 HP)
    //атаковать (нанесение урона 2 HP)
    public double attack() {
        int act = (0 + (int) (Math.random() * 2));
        int randBody = (0 + (int) (Math.random() * 7));
        switch (act) {
            case 0:
                saveLogStep("Нежить Лучник делает выстрел из лука" + bodyParts[randBody]);
                if (ellitGroup == true) {
                    ellitGroup = false;
                    return 4 * 1.5;
                } else return 4;
            default:
                saveLogStep("Нежить Лучник наносит удар кулаком" + bodyParts[randBody]);
                if (ellitGroup == true) {
                    ellitGroup = false;
                    return 2 * 1.5;
                } else return 2;
        }
    }

    public void damage(double damage) {
        this.heals -= damage;
    }

    public int health() {
        //System.out.println("Остаток здоровья Нежить Охотник: " + this.heals + "HP");
        return this.heals;
    }

    public String race() {
        return "Орда";
    }

    public void classHero() {
        saveLogStep("Нежить лучник");
    }

    public void deathHero() {
        saveLogStep("Нежить лучник погиб в бою\n");
    }

    public void buffToEliteHero() {
        ellitGroup = true;
    }

    public void deBuffToEliteHero() {
        ellitGroup = false;
    }

    public boolean buffCheck() {
        return ellitGroup;
    }

    public void deBuffHero() {
    }
}
