package race.orc;

import Interface.heroSkils;


import java.util.ArrayList;
import java.util.List;


public class orcSquad {
    public static List createOrcSquad() {
        List<heroSkils> list = new ArrayList<heroSkils>();
        list.add(new orcMage());
        list.add(new orcArcher());
        list.add(new orcArcher());
        list.add(new orcArcher());
        list.add(new orcWarrior());
        list.add(new orcWarrior());
        list.add(new orcWarrior());
        list.add(new orcWarrior());
        return list;
    }
}
