package race.orc;

import Interface.heroSkils;
import race.hordeHero;

import static race.saveLog.saveLogStep;

public class orcWarrior extends hordeHero implements heroSkils {
    //атака дубиной (нанесение урона 20 HP)
    public double attack() {
        int randBody = (0 + (int) (Math.random() * 7));
        saveLogStep("Орк Гоблик наносит удар дубиной" + bodyParts[randBody]);
        if (ellitGroup == true) {
            ellitGroup = false;
            return 20 * 1.5;
        } else return 20;
    }

    public void damage(double damage) {
        this.heals -= damage;
    }

    public int health() {
        return this.heals;
    }

    public String race() {
        return "Орда";
    }

    public void classHero() {
        saveLogStep("Орк гоблин");
    }

    public void deathHero() {
        saveLogStep("Орк гоблин погиб в бою\n");
    }

    public void buffToEliteHero() {
        ellitGroup = true;
    }

    public void deBuffToEliteHero() {
        ellitGroup = false;
    }

    public boolean buffCheck() {
        return ellitGroup;
    }

    public void deBuffHero() {
    }
}
