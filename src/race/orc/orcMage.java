package race.orc;

import Interface.heroSkils;
import race.hordeHero;

import static race.saveLog.saveLogStep;

public class orcMage extends hordeHero implements heroSkils {
    //наложение улучшения на персонажа своего отряда.
    //наложение проклятия (снятие улучшения с персонажа противника для следующего хода)
    public double attack() {
        int act = (0 + (int) (Math.random() * 2));
        switch (act) {
            case 0:
                saveLogStep("Орк Шаман накладывает улучшение на ");
                if (ellitGroup == true) {
                    ellitGroup = false;
                    return 0;
                } else return 0;
            default:
                saveLogStep("Орк Шаман накладывает проклятие на");
                if (ellitGroup == true) {
                    ellitGroup = false;
                    return -1;
                } else return -1;
        }
    }

    public void damage(double damage) {
        this.heals -= damage;
    }

    public int health() {
        return this.heals;
    }

    public String race() {
        return "Орда";
    }

    public void classHero() {
        saveLogStep("Орк шаман");
    }

    public void deathHero() {
        saveLogStep("Орк шаман погиб в бою\n");
    }

    public void buffToEliteHero() {
        ellitGroup = true;
    }

    public void deBuffToEliteHero() {
        ellitGroup = false;
    }

    public boolean buffCheck() {
        return ellitGroup;
    }

    public void deBuffHero() {
    }

}
