package race.orc;

import Interface.heroSkils;
import race.hordeHero;

import static race.saveLog.saveLogStep;

public class orcArcher extends hordeHero implements heroSkils {
    //стрелять из лука (нанесение урона 3 HP)
    //удар клинком (нанесение урона 2 HP)
    public double attack() {
        int act = (0 + (int) (Math.random() * 2));
        int randBody = (0 + (int) (Math.random() * 7));
        switch (act) {
            case 0:
                saveLogStep("Орк Лучник делает меткий выстрел из лука" + bodyParts[randBody]);
                if (ellitGroup == true) {
                    ellitGroup = false;
                    return 7 * 1.5;
                } else return 7;
            default:
                saveLogStep("Орк Лучник наносит удар топором" + bodyParts[randBody]);
                if (ellitGroup == true) {
                    ellitGroup = false;
                    return 5 * 1.5;
                } else return 5;
        }
    }

    public void damage(double damage) {
        this.heals -= damage;
    }

    public int health() {
        return this.heals;
    }

    public String race() {
        return "Орда";
    }

    public void classHero() {
        saveLogStep("Орк лучник");
    }

    public void deathHero() {
        saveLogStep("Орк лучник погиб в бою\n");
    }

    public void buffToEliteHero() {
        ellitGroup = true;
    }

    public void deBuffToEliteHero() {
        ellitGroup = false;
    }

    public void deBuffHero() {
    }

    public boolean buffCheck() {
        return ellitGroup;
    }
}
